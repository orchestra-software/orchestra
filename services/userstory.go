package main

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"
	"syscall/js"
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

type Priority int

const (
	UndefinedPriority Priority = iota
	MustDo
	ShouldDo
	CouldDo
)

type Risk int

const (
	UndefinedRisk Risk = iota
	Low
	Moderate
	High
)

type UserStoryId int

type UserStory struct {
	Id          UserStoryId `json:"id"`
	Description string      `json:"description"`
	Priority    Priority    `json:"priority"`
	Risk        Risk        `json:"risk"`
	Points      int         `json:"points"`
}

func (u *UserStory) UnmarshalJSON(data []byte) error {
	allowedPriorities := map[string]Priority{
		"Must Do":   MustDo,
		"Should Do": ShouldDo,
		"Could Do":  CouldDo,
	}

	allowedRisks := map[string]Risk{
		"Low":      Low,
		"Moderate": Moderate,
		"High":     High,
	}
	var v interface{}
	if err := json.Unmarshal(data, &v); err != nil {
		fmt.Println("raised err:", err)
		return err
	}
	m := v.(map[string]interface{})

	var id int
	switch v := m["id"].(type) {
	default:
		fmt.Printf("unexpected type %T", v)
	case float64:
		id = int(m["id"].(float64))
	case string:
		id, _ = strconv.Atoi(m["id"].(string))
	}
	u.Id = UserStoryId(id)
	u.Description, _ = m["description"].(string)
	u.Priority = allowedPriorities[strings.Trim(m["priority"].(string), " ")]
	u.Risk = allowedRisks[(m["risk"].(string))]
	var points int
	switch v := m["points"].(type) {
	default:
		fmt.Printf("unexpected type %T", v)
	case float64:
		points = int(m["points"].(float64))
	case string:
		points, _ = strconv.Atoi(m["points"].(string))
	}
	u.Points = points
	return nil
}

type UserStoryRecord = UserStory

type UserStoryListNode struct {
	*UserStory
	prev *UserStoryListNode
	next *UserStoryListNode
}

type UserStoryList struct {
	Length   int
	head     *UserStoryListNode
	tail     *UserStoryListNode
	iterator *UserStoryListNode
}

func (u *UserStory) GetPriorityDesc() string {
	priorityMapString := map[Priority]string{
		UndefinedPriority: "Undefined",
		MustDo:            "Must Do",
		ShouldDo:          "Should Do",
		CouldDo:           "Could Do",
	}
	return priorityMapString[u.Priority]
}

func (u *UserStory) Print() {

	riskMapString := map[Risk]string{
		UndefinedRisk: "Undefined",
		High:          "High",
		Moderate:      "Moderate",
		Low:           "Low",
	}
	fmt.Println("|\t", u.Id, "\t|\t", u.Points, "pts\t|\t", u.GetPriorityDesc(), "\t|\t", riskMapString[u.Risk], "\t|\t", u.Description, "\t|")
}

func (l *UserStoryList) GetFirst() *UserStory {
	l.iterator = l.head
	if l.iterator == nil {
		return nil
	}
	tmp := l.iterator.UserStory
	return tmp
}

func (l *UserStoryList) GetNext() *UserStory {
	if l.iterator.next == nil {
		l.iterator = nil
		return nil
	}
	l.iterator = l.iterator.next
	return l.iterator.UserStory
}

func (l *UserStoryList) RemoveCurrent() {
	if l.iterator == l.head {
		// delete head
		if l.head.next != nil {
			l.head.next.prev = nil
		}
		l.head = l.head.next
	} else if l.iterator == nil {
		// delete tail
		fmt.Println("> attempting to remove (tail) ", l.tail.Id)
		if l.tail.prev != nil {
			l.tail.prev.next = nil
		}
		l.tail = l.tail.prev
	} else {
		// delete generic node
		if l.iterator.prev != nil {
			l.iterator.prev.next = l.iterator.next
		}
		if l.iterator.next != nil {
			l.iterator.next.prev = l.iterator.prev
		}
	}

	l.Length--
}

func makeUserStoryList() *UserStoryList {
	fmt.Println("makeUserStoryList")
	return &UserStoryList{}
}

func makeUserStoryListNode(userStory UserStory) *UserStoryListNode {
	return &UserStoryListNode{UserStory: &userStory}
}

func (l *UserStoryList) push(userStory UserStory) {
	l.pushNode(makeUserStoryListNode(userStory))
}

func (l *UserStoryList) pushNode(newNode *UserStoryListNode) {
	if l.head == nil {
		l.head = newNode
		l.tail = newNode
		l.iterator = l.head
	} else {
		currentNode := l.head
		for currentNode.next != nil {
			currentNode = currentNode.next
		}
		currentNode.next = newNode
		newNode.prev = currentNode
		l.tail = currentNode.next
	}
	fmt.Println("")
	fmt.Println("added id:", newNode.Id)
	if newNode.prev != nil {
		fmt.Println("after id:", newNode.prev.Id)
	}
	l.Length++
}

func (l *UserStoryList) reverse() {
	currentNode := l.head
	var nextInList *UserStoryListNode
	l.head, l.tail = l.tail, l.head
	for currentNode != nil {
		nextInList = currentNode.next
		currentNode.next, currentNode.prev = currentNode.prev, currentNode.next
		currentNode = nextInList
	}
}

func (l *UserStoryList) SortByRiskAsc() {
	l.quickSortByRisk(l.head, l.tail)
	l.Print()
}

func (l *UserStoryList) SortByRiskDec() {
	l.quickSortByRisk(l.head, l.tail)
	l.reverse()
}

func (l *UserStoryList) Print() {
	for s := l.GetFirst(); s != nil; s = l.GetNext() {
		s.Print()
	}
}

func (l UserStoryList) partitionByRisk(head, tail *UserStoryListNode) *UserStoryListNode {
	var current *UserStoryListNode = head
	var location *UserStoryListNode = head.prev
	var temp Risk = 0
	for current != nil && current != tail {
		if current.UserStory.Risk <= tail.UserStory.Risk {
			if location == nil {
				location = head
			} else {
				location = location.next
			}
			temp = current.UserStory.Risk
			current.UserStory.Risk = location.UserStory.Risk
			location.UserStory.Risk = temp
		}
		current = current.next
	}
	if location == nil {
		location = head
	} else {
		location = location.next
	}
	temp = tail.UserStory.Risk
	tail.UserStory.Risk = location.UserStory.Risk
	location.UserStory.Risk = temp
	return location
}

func (l *UserStoryList) quickSortByRisk(head, tail *UserStoryListNode) {
	if head != tail && head != nil &&
		tail != nil && tail.next != head {
		var node *UserStoryListNode = l.partitionByRisk(head, tail)
		if node != nil {
			l.quickSortByRisk(node.next, tail)
			l.quickSortByRisk(head, node.prev)
		}
	}
}

func (l *UserStoryList) ToSlice() []UserStory {
	var slice []UserStory
	for s := l.GetFirst(); s != nil; s = l.GetNext() {
		slice = append(slice, *s)
	}
	return slice
}

func createListsFromCSV(data [][]string) (*UserStoryList, *UserStoryList, *UserStoryList) {
	fmt.Println(data)
	allowedPriorities := map[string]Priority{
		"Must Do":   MustDo,
		"Should Do": ShouldDo,
		"Could Do":  CouldDo,
	}

	allowedRisks := map[string]Risk{
		"Low":      Low,
		"Moderate": Moderate,
		"High":     High,
	}

	getMyList := map[Priority]*UserStoryList{
		MustDo:   makeUserStoryList(),
		ShouldDo: makeUserStoryList(),
		CouldDo:  makeUserStoryList(),
	}
	for i, line := range data {
		fmt.Println("Line: ", line)
		if i > 0 { // omit header line
			var rec UserStoryRecord
			for j, field := range line {
				switch j {
				case 0:
					v, _ := strconv.Atoi(field)
					rec.Id = UserStoryId(v)
					break
				case 1:
					rec.Description = field
					break
				case 2:
					rec.Priority = allowedPriorities[strings.Trim(field, " ")]
					break
				case 3:
					rec.Risk = allowedRisks[field]
					break
				case 4:
					rec.Points, _ = strconv.Atoi(field)
					break
				}
			}

			fmt.Println("rec.Priority:", rec.Priority)
			tmpList := getMyList[rec.Priority]
			fmt.Println("tmpList: ", tmpList)
			tmpList.push(rec)
		}
	}
	return getMyList[MustDo], getMyList[ShouldDo], getMyList[CouldDo]
}

func createListsFromJSON(data string) (*UserStoryList, *UserStoryList, *UserStoryList) {
	getMyList := map[Priority]*UserStoryList{
		MustDo:   makeUserStoryList(),
		ShouldDo: makeUserStoryList(),
		CouldDo:  makeUserStoryList(),
	}
	var userStories []UserStory
	err := json.Unmarshal([]byte(data), &userStories)
	if err != nil {
		fmt.Println(err)
	}

	for _, u := range userStories {
		tmpList := getMyList[u.Priority]
		tmpList.push(u)
	}

	return getMyList[MustDo], getMyList[ShouldDo], getMyList[CouldDo]
}

func checkDependencies(dependenciesMap map[UserStoryId]*UserStoryList, satisfiedDependencies map[UserStoryId]bool, u *UserStory) bool {
	uds := dependenciesMap[u.Id]
	ok := uds == nil || uds.Length == 0
	if !ok {
		// check if all dependencies of u are satisfied
		for ud := uds.GetFirst(); ud != nil; ud = uds.GetNext() {
			if !satisfiedDependencies[ud.Id] {
				fmt.Println("UserStory", u.Id, "still needs", ud.Id, "to be implemented")
				return false
			}
		}
	}
	return ok
}

func createDependencies(userStories []UserStory, dependenciesData map[UserStoryId][]UserStoryId) map[UserStoryId]*UserStoryList {
	var dependencies map[UserStoryId]*UserStoryList

	return dependencies
}

type Sprint struct {
	UserStories []UserStory `json:"userStories"`
	Points      int         `json:"points"`
}

func getSprintsWrapper() js.Func {
	getSprintsFunc := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		b, err := json.Marshal(GetSprints(args[0].String()))
		if err != nil {
			fmt.Println(err)
		}
		return string(b)
	})
	return getSprintsFunc
}

func getSprintsFromFileWrapper() js.Func {
	getSprintsFunc := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		b, err := json.Marshal(GetSprintsFromFile([]byte(args[0].String())))
		if err != nil {
			fmt.Println(err)
		}
		return string(b)
	})
	return getSprintsFunc
}

func main() {
	fmt.Println("Go WebAssembly: loaded UserStory.go")
	js.Global().Set("GetSprints", getSprintsWrapper())
	js.Global().Set("GetSprintsFromFile", getSprintsFromFileWrapper())
	<-make(chan bool)
}

func GetSprintsFromFile(inputFile []byte) []Sprint {
	f := bytes.NewReader(inputFile)
	csvReader := csv.NewReader(f)
	data, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal(err)
	}

	mustDoStories, shouldDoStories, couldDoStories := createListsFromCSV(data)

	return GetSprintsRoutine(mustDoStories, shouldDoStories, couldDoStories, 20)
}

func GetSprints(inputJSON string) []Sprint {
	mustDoStories, shouldDoStories, couldDoStories := createListsFromJSON(inputJSON)
	return GetSprintsRoutine(mustDoStories, shouldDoStories, couldDoStories, 20)
}

func GetSprintsRoutine(mustDoStories *UserStoryList, shouldDoStories *UserStoryList, couldDoStories *UserStoryList, teamVelocity int) []Sprint {
	// temporary merged lists
	test := map[UserStoryId]*UserStory{}
	var userStories []UserStory
	for s := mustDoStories.GetFirst(); s != nil; s = mustDoStories.GetNext() {
		userStories = append(userStories, *s)
		test[s.Id] = s
	}
	for s := shouldDoStories.GetFirst(); s != nil; s = shouldDoStories.GetNext() {
		userStories = append(userStories, *s)
		test[s.Id] = s
	}

	for s := couldDoStories.GetFirst(); s != nil; s = couldDoStories.GetNext() {
		userStories = append(userStories, *s)
		test[s.Id] = s
	}

	// dependencies := createDependencies(userStories, dependenciesData)
	dependencies := map[UserStoryId]*UserStoryList{}
	for userStoryId, cDependencies := range dependencies {
		//map[UserStoryId]*UserStoryList{}
		for c := cDependencies.GetFirst(); c != nil; c = cDependencies.GetNext() {
			fmt.Println(">>>> The user story:", userStoryId, " (of Priority: ", test[userStoryId].GetPriorityDesc(), ") needs the story", c.Id, " (of Priority: ", c.GetPriorityDesc(), ") to be completed")
		}
	}
	satisfiedDependencies := map[UserStoryId]bool{}

	fmt.Println(dependencies)

	mustDoStories.SortByRiskDec()
	shouldDoStories.SortByRiskDec()
	couldDoStories.SortByRiskAsc()
	// fmt.Println("mustDoStories: ", mustDoStories.Length)
	// fmt.Println("shouldDoStories: ", shouldDoStories.Length)
	// fmt.Println("couldDoStories: ", couldDoStories.Length)

	lists := [3]*UserStoryList{mustDoStories, shouldDoStories, couldDoStories}

	// iterate  sprints
	// var sprints [][]UserStory
	var sprints []Sprint

	for sp := 1; sp <= 4; sp++ {
		// var sprint []UserStory
		var sprint Sprint
		// var sprintPoints int = 0
		// iterate stories

		var storiesBeforeCycles int
		for i := 0; i < len(lists); i++ {
			var l *UserStoryList = lists[i]
		RETRY_MUSTDO:
			storiesBeforeCycles = l.Length
			for s := l.GetFirst(); s != nil; s = l.GetNext() {
				if sprint.Points+s.Points <= teamVelocity {
					if checkDependencies(dependencies, satisfiedDependencies, s) {
						satisfiedDependencies[s.Id] = true
						sprint.UserStories = append(sprint.UserStories, *s)
						sprint.Points += s.Points
						l.RemoveCurrent()
						if sprint.Points == teamVelocity {
							goto SPRINT_END
						}
					}

				} else {
					// fmt.Println("exceeding points")
				}
			}
			if l.Length > 0 && l.Length != storiesBeforeCycles {
				goto RETRY_MUSTDO
			}
		}

	SPRINT_END:
		// fmt.Println("num of stories: ", len(sprint.UserStories))
		// fmt.Println("points: ", sprint.Points)

		sprints = append(sprints, sprint)
	}

	// fmt.Println("num of stories left in mustDoStories: ", mustDoStories.Length)
	// fmt.Println("num of stories left in shouldDoStories: ", shouldDoStories.Length)
	// fmt.Println("num of stories left in couldDoStories: ", couldDoStories.Length)

	var notImplementedStories []UserStory
	var sum = 0
	for s := mustDoStories.GetFirst(); s != nil; s = mustDoStories.GetNext() {
		notImplementedStories = append(notImplementedStories, *s)
		sum += s.Points
	}
	for s := shouldDoStories.GetFirst(); s != nil; s = shouldDoStories.GetNext() {
		notImplementedStories = append(notImplementedStories, *s)
		sum += s.Points
	}
	for s := couldDoStories.GetFirst(); s != nil; s = couldDoStories.GetNext() {
		notImplementedStories = append(notImplementedStories, *s)
		sum += s.Points
	}

	fmt.Println("## Release Planning")
	fmt.Println("| Id   |      Points      |  Priority | Risk |  User Story| Dependencies |")
	fmt.Println("|:----------:|:-------------:|:------:|:------:|:------:|------:|")

	for i, sprint := range sprints {
		fmt.Println("|\t**Sprint**", i+1, "\t|\t\t|\t\t|\t\t|\t\t|\t\t|")
		for _, s := range sprint.UserStories {
			s.Print()
		}
		fmt.Println("|\t**Total**\t|\t", sprint.Points, " pts\t|\t\t|\t\t|\t\t|\t\t")
	}

	fmt.Println("|\t**Backlog**\t|\t|\t|\t|\t|\t\t|\t\t|")
	for _, s := range notImplementedStories {
		s.Print()
	}
	fmt.Println("|\t**Total**\t|\t", sum, " pts\t|\t\t|\t\t|\t\t|\t\t|")

	return sprints
}
