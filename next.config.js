/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: true,
    // assetPrefix: process.env.NODE_ENV === 'production' ? '/website' : '',
    images: {
        loader: 'imgix',
        path: '/',
    },
    experimental: {
        esmExternals: false
    }
}

module.exports = nextConfig