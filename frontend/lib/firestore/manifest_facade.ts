
import {
    collection, setDoc, deleteDoc, doc,
    query, orderBy,
limit, getDocs, where, serverTimestamp,
getDoc,
updateDoc,
arrayUnion
} from 'firebase/firestore';
import { firestore } from '../../firebase'


const COLLECTION_MANIFESTS = "manifests"

const manifestsCollection = collection(firestore, "manifests");
const getPublicManifestsQuery = () => query(
        manifestsCollection,
        where("visibility", "==", "public"),
        orderBy("createdAt"),
        limit(25)
);

const getMyManifestsQuery = ({user}) => {
    return query(
        manifestsCollection,
        where("createdBy.userUid", "==", user.uid),
        orderBy("createdAt"),
        limit(25)
    );
};

export async function getDocsManifests({user}) {
    return await new Promise(async (resolve) => {
        await Promise.all([
            new Promise(async (resolve1) => {
                const documents = await getDocs(getPublicManifestsQuery())
                resolve1(documents);
            }),
            new Promise(async (resolve2) => {
                const documents = await getDocs(getMyManifestsQuery({user}))
                resolve2(documents)
            }),
        ]).then(docs => {
            // first flat() is for the result of Promise.all
            // second flat() is for the merging collections from different queries
            // @ts-ignore
            console.log((docs.flat()).docs);
            // @ts-ignore
            resolve(docs.flat().map(x=>x.docs).flat().map(x => ({
                id: x.id,
                ...x.data()
            })).sort((a,b) => {
                return b.createdAt - a.createdAt; 
            }).map(x => ({
                id: x.id,
                name: x.name,
                description: x.description,
            })));
        });
    });
}

type GetDocManifestArgs = {
    slug: string;
}
export async function getDocManifest({ slug }: GetDocManifestArgs) {
    const docSnap = await getDoc(doc(firestore, "manifests", encodeURI(slug)))

    if (docSnap.exists()) {
        console.log("Document data:", docSnap.data());
        return docSnap.data()
      } else {
        // doc.data() will be undefined in this case
        console.log("No such document!");
      }

    return null;
}

export type AddDocManifestArgs = {
    user,
    slug: string;
    name: string;
    description: string;
}
export async function addDocManifest(args: AddDocManifestArgs) {
    console.log("Firestore: ", firestore)
    const docRef = await setDoc(doc(
        manifestsCollection,
        encodeURIComponent(args.slug)
        ),
        {
            name: args.name,
            description: args.description,
            urls: [],
            createdBy: {
                userUid: args.user.uid,
                userDisplayName: args.user.displayName
            },
            createdAt:  serverTimestamp(),
            visibility: "private",
            diagrams: []
        }
    );
    
    // @ts-ignore
    if (docRef)
        return {
            status: "success",
            docRef: docRef
        }

    return {
        status: "error"
    }
    // this id must be assigned to somewhere related to 
    // the user to store the id to be deleted
}


type UpdateDocManifestWithNewUserStoryStorageArgs = {
    manifestId: string;
    apiEndpoint: string;
    projectPath: string;
    projectAccessToken: string;
}
export async function updateDocManifestWithNewUserStoryStorage(args: UpdateDocManifestWithNewUserStoryStorageArgs) {
    console.log(args);
    const docRef = doc(firestore, "manifests", args.manifestId);

    await updateDoc(
        docRef, 
        {
            userStoryStorages: arrayUnion({
                type: "gitlab",
                apiEndpoint: args.apiEndpoint,
                projectPath: args.projectPath,
                projectAccessToken: args.projectAccessToken
            })
        }
    );
}

export type DeleteDocManifestArgs = {
    manifestId: string;
}

export async function deleteDocManifest(args: DeleteDocManifestArgs) {
    await deleteDoc(doc(firestore, COLLECTION_MANIFESTS, args.manifestId))
}

export async function editDocManifest() {

}