import { useState, useCallback, SyntheticEvent } from "react";
import Head from "next/head";
import { useRouter } from "next/router";
import Layout, { siteTitle } from "../components/layout";
import {
  Tabs,
  Form,
  Input,
  Button,
  Spinner,
} from "@canonical/react-components";
import { useUser, useSigninCheck } from "reactfire";

export default function LoginNeeded({ children }) {
  const router = useRouter();
  // const { data: user } = useUser();
  const { status, data: signInCheckResult } = useSigninCheck();

  const handleLoginButtonClick = useCallback(() => {
    if (!router) return;
    router.push(`/auth/sign-in`);
  }, [router]);

  if (status == "loading") return <div>loading</div>;

  if (signInCheckResult?.signedIn) {
    return children;
  }

  return (
    <Layout pageTitle="Login needed">
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <Button onClick={handleLoginButtonClick}>Login</Button>
    </Layout>
  );
}
