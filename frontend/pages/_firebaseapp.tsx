import React from 'react';
import { AuthProvider, FirebaseAppProvider, FirestoreProvider } from "reactfire";
import { app, auth, firestore } from "../firebase";

function AuthMiddleware({children}) {
    return <AuthProvider sdk={auth}>{children}</AuthProvider>
}

function FirestoreMiddleware({children}) {
    return <FirestoreProvider sdk={firestore}>
        {children}
    </FirestoreProvider>
}

export default function FirebaseApp({children}) {

    return <FirebaseAppProvider firebaseApp={app}>
        <AuthMiddleware>
            <FirestoreMiddleware>
            {children}
            </FirestoreMiddleware>
        </AuthMiddleware>    
    </FirebaseAppProvider>
}