import { useAuth } from 'reactfire'
import Layout from "../components/layout"

export default function MePage() {
  const auth = useAuth();

  return (
    <Layout>
      <pre>{JSON.stringify(auth, null, 2)}</pre>
    </Layout>
  )
}