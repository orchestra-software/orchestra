import { useState, useCallback, SyntheticEvent } from 'react';
import Head from 'next/head'
import { useRouter } from 'next/router'
import Layout, { siteTitle } from "../../components/layout";
import { Tabs, Form, Input, Button, Spinner } from '@canonical/react-components'
import { useUser, useSigninCheck } from 'reactfire'

import { addDocManifest } from '../../lib/firestore/manifest_facade';
import LoginNeeded from '../_login-needed';
export default function NewManifest() {
    const { data: user } = useUser();
    const { status, data: signInCheckResult } = useSigninCheck();
    const router = useRouter();
    const [isLoading, setLoading] = useState(false);    

    const [isManifestSlugManuallyChanged, setManifestSlugManuallyChanged] = useState(false);
    const [formData, setFormData] = useState({
        manifestName: "",
        manifestDescription: "",
        manifestSlug: ""
    });

    const handleManifestNameChange = useCallback((e) => {
        console.log(e);

        setFormData({
            ...formData,
            manifestName: e.target.value,
            manifestSlug: !isManifestSlugManuallyChanged 
                ? e.target.value.toLowerCase().replaceAll(/ /ig, '-')
                : formData.manifestSlug
        });
    }, [formData, isManifestSlugManuallyChanged]);

    const handleManifestSlugChange = useCallback((e) => {
        console.log(e);
        setManifestSlugManuallyChanged(e.target.value != "");
        setFormData({
            ...formData,
            manifestSlug: e.target.value
        });
    }, [formData]);

    const handleManifestDescriptionChange = useCallback((e) => {
        console.log(e);
        setFormData({
            ...formData,
            manifestDescription: e.target.value
        });
    }, [formData]);

    const handleNewManifestButtonClick = useCallback(async (e: SyntheticEvent) => {
        e.preventDefault();
        if (!(formData && user && router)) {
            return;
        }
        const { status, docRef } = await addDocManifest({
            user,
            name: formData.manifestName,
            slug: formData.manifestSlug,
            description: formData.manifestDescription
        });

        console.log("status:", status, docRef);

        if (status == "success") {
            // @ts-ignore
            router.push(`/manifests/${docRef.id}`) 
        }
    }, [formData, user, router]);
    

    return <LoginNeeded><Layout
        pageTitle="New Manifest (still work in progress)"
        breadcrumbs={[
            {
                label: "Manifests",
                route: "/manifests"
            },
            {
                label: "New"
            }
        ]}
        >
            <Head>
                <title>{siteTitle}</title>
            </Head>
        <Tabs links={[
            {
                active: true,
                label: "Form",
            }
        ]} />
        <Form>
            <fieldset>
                {isLoading && <Spinner />}
                <h2>Create new manifest</h2>
                <div className="p-form--inline">
                    <Input
                        value={formData.manifestName}
                        type="text"
                        label="Name"
                        onChange={handleManifestNameChange}
                    />
                    <Input
                        value={formData.manifestSlug}
                        type="text"
                        label="Manifest Slug"
                        onChange={handleManifestSlugChange}
                    />
                    <Input
                        value={formData.manifestDescription}
                        type="text"
                        label="Description"
                        onChange={handleManifestDescriptionChange}
                    />
                    <>Visibility: private</>
                    <Button onClick={handleNewManifestButtonClick}>create new</Button>
                    
                    </div>
            </fieldset>
        </Form>
    </Layout></LoginNeeded>
}   