import Head from "next/head";
import Layout, { siteTitle } from "../../components/layout";
import { useCallback, useEffect } from "react";
import { useRouter } from "next/router";
import { GoogleAuthProvider } from "firebase/auth";
import { Form, Button } from "@canonical/react-components";
import { useSignInWithProvider } from "../../hooks/useSignInWithProvider";

const SignIn = () => {
  const [signInWithProvider, signInWithProviderState] = useSignInWithProvider();
  const router = useRouter();

  const AuthGoogleProviderButton = () => {
    return (
      <Button
        className="rounded-lg p-2 font-bold bg-red-400 text-white"
        onClick={() => {
          console.log("before sign in with provider");
          signInWithProvider(new GoogleAuthProvider());
        }}
      >
        Google
      </Button>
    );
  };

  const onSignIn = useCallback(() => {
    return router.push("/dashboard");
  }, [router]);

  useEffect(() => {
    if (signInWithProviderState.success) {
      onSignIn();
    }
  }, [signInWithProviderState.success, onSignIn]);

  return (
    <Layout>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <div className="flex flex-col space-y-8 items-center justify-center mx-auto h-screen w-11/12 lg:w-4/12">
        <div className="flex flex-col space-y-8">
          <Form>
            <fieldset>
              <p>Sign in with</p>
              <AuthGoogleProviderButton />
            </fieldset>
          </Form>
        </div>
      </div>
    </Layout>
  );
};

export default SignIn;
