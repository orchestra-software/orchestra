import Layout from "../../components/layout";
import { Card, Button } from '@canonical/react-components'
import Link from 'next/link'

export default function Tools() {
    return <Layout pageTitle="Tools">        
            <Card title="Sprints" style={{
                marginLeft: '1.5rem'
            }}>
                <p>
                Simple Golang application to generate sprints specifying the team velocity. It accepts a set of user stories and then sort them by priority and risk.
                Since it's compiled to WebAssembly it can be used in the browser.
                </p>

                <p><Link href="/tools/sprints">
                    <Button>Open</Button>
                </Link>
                </p>
            </Card>
    </Layout>
}