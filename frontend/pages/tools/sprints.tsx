import React, { Fragment, SyntheticEvent, useCallback, useReducer, useState } from 'react';
import Head from 'next/head'
import Layout, { siteTitle } from '../../components/layout';
import DropZone from "../../components/DropZone";
import Script from 'next/script'
import { Button, Form, MainTable } from '@canonical/react-components'

import { RiskText, PriorityText } from '../../components/UserStory';
import { Tabs, Textarea } from '@canonical/react-components'

declare global {
  interface Window {
    GetSprints: Function;
    GetSprintsFromFile: Function;
    Go: any;
  }
}

enum InputMode {
  JSON = "json",
  CSVFile = "csvfile"
}

const TEST_VALUE = `[
  {
    "id": 1,
    "priority": "Must Do", 
    "risk": "High",
    "points": 3
  }
]`;

const INPUT_STRING_DEFAULT = TEST_VALUE;

export default function ReleasePlan() {
  const reducer = (state, action) => {
    switch (action.type) {
      case "SET_IN_DROP_ZONE":
        return { ...state, inDropZone: action.inDropZone };
      case "ADD_FILE_TO_LIST":
        return { ...state, fileList: state.fileList.concat(action.files) };
      case "DISPLAY_RELEASE_PLAN":
        setShouldShowFileSection(false);
        return {
          ...state,
          releasePlan: action.releasePlan
        }
      default:
        return state;
    }
  };

  // destructuring state and dispatch, initializing fileList to empty array
  const [shouldShowFileSection, setShouldShowFileSection] = useState(true);
  const [data, dispatch] = useReducer(reducer, {
    inDropZone: false,
    fileList: [],
    releasePlan: {
      sprints: []
    }
  });

  const [inputMode, setInputMode] = useState<InputMode>(InputMode.JSON);
  const [isValidJSONInput, setValidJSONInput] = useState<boolean>(true);

  const [inputString, setInputString] = useState<string>(INPUT_STRING_DEFAULT);

  const onSubmitHandler = useCallback((e: SyntheticEvent) => {
    if (inputMode == InputMode.JSON) {
      e.preventDefault();
      var rawSprints = window.GetSprints(inputString);
      var sprints = JSON.parse(rawSprints);
      console.log(sprints);
      var releasePlan = {
        sprints: sprints
      };
      dispatch({ type: "DISPLAY_RELEASE_PLAN", releasePlan });
    } else if (inputMode == InputMode.CSVFile) {
      e.preventDefault();
      let files = data.fileList;

      files[0].text().then((x) => {
        var rawSprints = window.GetSprintsFromFile(x);
        console.log(rawSprints);
        var sprints = JSON.parse(rawSprints);
        console.log(sprints);
        var releasePlan = {
          sprints: sprints
        };
        dispatch({ type: "DISPLAY_RELEASE_PLAN", releasePlan });
      });
    }
  }, [inputMode]);

  return (
    <Layout pageTitle="Sprints" headerButtonId="release-plan">
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <Script src="/lib/wasm_exec.js" onLoad={() => {
        console.log("loaded wasm");
        const go = new window.Go();
        WebAssembly.instantiateStreaming(
          fetch("/lib/releaseplan.wasm"),
          go.importObject
        )
          .then((result) => {
            go.run(result.instance);
          });
      }} />
      {shouldShowFileSection && <section>
        <Form>
          <fieldset>
            <h2>Create your release plan</h2>
            <Tabs links={[
              {
                active: inputMode == InputMode.JSON,
                label: "From JSON",
                onClick: () => {
                  setInputMode(InputMode.JSON);
                }
              },
              {
                active: inputMode == InputMode.CSVFile,
                label: "From CSV file",
                onClick: () => {
                  setInputMode(InputMode.CSVFile);
                }
              }
            ]} />
            {inputMode == InputMode.CSVFile && <>
              <p className="p-form-help-text" id="exampleInputHelpMessage">
                Upload your CSV file with user stories. It must have the headers: `Id`, `Description`, `Priority`, `Risk`, `Points`
              </p>
              <DropZone data={data} dispatch={dispatch} />
              <div className="u-align--center">
                <Button appearance="positive" onClick={onSubmitHandler} disabled={data.fileList.length <= 0}>Create</Button>
              </div>
            </>}

            {inputMode == InputMode.JSON && <>
              <Textarea rows={20} defaultValue={INPUT_STRING_DEFAULT} onChange={(v) => {
                setInputString(v.target.value);
              }}></Textarea>
              <Button appearance="positive" onClick={onSubmitHandler} disabled={!isValidJSONInput}>Create</Button>
            </>}

          </fieldset>
        </Form>
      </section>
      }
      {
        data.releasePlan.sprints.map((sprint, sprintIdx) => {

          const rows = [];
          (sprint.userStories || []).forEach((userStory) => {
            rows.push({
              columns: [{
                content: userStory.id,
                role: "rowheader"
              }, {
                content: userStory.description,
                className: "u-align--right"
              }, {
                content: userStory.points,
                className: "u-align--right"
              }, {
                content: PriorityText[userStory.priority],
                className: "u-align--right"
              }, {
                content: RiskText[userStory.risk],
                className: "u-align--right"
              }]
            });
          });

          return <Fragment key={sprintIdx}>
            <h3>Sprint #{sprintIdx + 1}</h3>
            <h4>Total Points: {sprint.points}</h4>
            <MainTable headers={[{
              content: <span>
                Id <i className="p-icon--information"></i>
              </span>,
              heading: "Id"
            }, {
              content: "User Story",
              className: "u-align--right"
            }, {
              content: "Points",
              className: "u-align--right"
            }, {
              content: "Priority",
              className: "u-align--right"
            }, {
              content: "Risk",
              className: "u-align--right"
            }]}
              rows={rows} responsive />
          </Fragment>
        })

      }

    </Layout>
  )
}