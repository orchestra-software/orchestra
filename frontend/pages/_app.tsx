import React from 'react';
import Script from 'next/script'
import { AppProps } from 'next/app';
import {
  ClientContext,
  GraphQLClient
} from 'graphql-hooks'

import '../styles/global.css'
import '../styles/main.scss';

import FirebaseApp from './_firebaseapp';

declare global {
      interface Window { dataLayer: any; }
}


const client = new GraphQLClient({
  url: 'https://gitlab.com/api/graphql'
});

export default function App(props: AppProps) {
  const { Component, pageProps } = props;

  return <ClientContext.Provider value={client}>
     <Script
             src="https://www.googletagmanager.com/gtag/js?id=G-GZCX4Q3TXH"
                     strategy="afterInteractive"
                           />
                                 <Script id="google-analytics" strategy="afterInteractive">
                                         {`
                                                   window.dataLayer = window.dataLayer || [];
                                                             function gtag(){window.dataLayer.push(arguments);}
                                                                       gtag('js', new Date());
                                                                                 gtag('config', 'G-GZCX4Q3TXH');
                                                                                         `}
                                                                                               </Script>
    <FirebaseApp >
      <Component {...pageProps} />
    </FirebaseApp>
  </ClientContext.Provider>
}
