import {
  Strip,
  Card,
  Button,
  Row,
  Col,
  ButtonAppearance,
} from "@canonical/react-components";
import Link from "next/link";

export default function Home({ allPostsData }) {
  return (
    <>
      <header id="navigation" className="p-navigation is-dark">
        <a className="p-navigation__link">
          <i>Orchestra</i>.<small>software</small>
        </a>
      </header>
      <section>
        <Strip light bordered>
          <h1>Bring everything together. Focus on what matters.</h1>
          <div className="u-align--right">
            <img src="/assets/undraw_operating_system_re_iqsc.svg" alt="" />
          </div>
          <h6>
            Systems are composed by many entities, projects, repositories,
            documents and tools. Centralize your resources into one Manifest.
            <br />
            Give your software the knowledge base it deserves.
          </h6>
        </Strip>
        <Strip light deep bordered>
          <h1>A sharp picture of the software. Anytime.</h1>
          <div className="u-align--center">
            <img
              src="/assets/undraw_scrum_board_re_wk7v.svg"
              className=""
              alt=""
            />
          </div>
          <p>
            Software products change commit by commit. Be aware of what your
            software is.
          </p>
        </Strip>
        <Strip light deep bordered>
          <h1>Plan the future of your software products</h1>
          <div className="u-align--center">
            <img
              src="/assets/undraw_schedule_re_2vro.svg"
              className=""
              alt=""
            />
          </div>
          <p>
            Align requirements, user stories, sprints, release plans on your
            vision.
          </p>
        </Strip>
        <Strip light deep>
          <div className="u-align--center">
            <h1>Use cases</h1>
          </div>
          <Card title={"Developers"}></Card>
          <Card title={"Managers"}></Card>
          <Card title={"QAs and Testers"}></Card>
          <p></p>
        </Strip>
      </section>
      <Strip dark deep style={{ backgroundColor: "#930139", color: "white" }}>
        <div className="u-align--center">
          <h1>Beta Launch</h1>
        </div>
        <Row>
          <div className="u-align--center">
            <Link href="https://bea18304.sibforms.com/serve/MUIEALXfEjr1Te226-sgsRgZOPk_bXrT49i7YAEdyuMRgsspLkCtvXEWZlvTY7bRXDXhqvqiZo78_WWstfxL9_CClB6n_zOXkjflD3jksLZmfzg__dYfyP-MDxJbdo_I4vRRUH6ww1HEblXcJj56Eap24Rg2j64_wegbxYqKjE4f_I7BhjjiWt9PL27orvwLExPxWL_-Jne4xRyv">
              <Button appearance={ButtonAppearance.POSITIVE}>Newsletter</Button>
            </Link>
          </div>
        </Row>
        <Row>
          <div className="u-align--center">
            <Link href="https://calendly.com/fabrizio-armango/orchestra-software-demo">
              <Button>Book a demo</Button>
            </Link>
          </div>
        </Row>
      </Strip>
      <footer
        style={{ backgroundColor: "#111111", color: "#888888" }}
        className="l-footer p-strip--light u-no-padding--top u-no-padding--bottom"
      >
        <nav className="row" aria-label="Footer">
        <p><small>Open-source illustrations from <a href="https://undraw.co/">unDraw</a></small></p>
        </nav>
        <nav className="row" aria-label="Footer">
          <div className="u-align--center">
            v{process.env.NEXT_PUBLIC_ORCHESTRA_VERSION}
            <span className="u-off-screen">
              <a href="#">Go to the top of the page</a>
            </span>
          </div>
        </nav>
      </footer>
    </>
  );
}
