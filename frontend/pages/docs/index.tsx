import Layout from "../../components/layout"
import { Card } from '@canonical/react-components'

export default function Docs() {
    return <Layout pageTitle="Documentation">        
            <Card title="Manifest" style={{
                marginLeft: '1.5rem'
            }}>
                <p>
                Il <i>Manifest</i> è un contenitore logico per progetti e membri, costituisce uno spazio di lavoro che comprende uno o più progetti che costituiscono un sistema software.
                </p>
                <p>
                In un manifest possono essere definiti dei ruoli e associati dei membri.
                </p>
                <p>
                Diversi manifest possono essere referenziati da altri manifest per la composizione di macro sistemi più complessi.
                </p>
            </Card>
    </Layout>
}