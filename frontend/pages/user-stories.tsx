import React, { useContext, useCallback, useEffect, useReducer, useState } from 'react';
import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'
import { Chip, Button, Form, Input, Select } from '@canonical/react-components'
import UserStory, { UserStoryRisk, UserStoryPriority } from '../components/UserStory';
import {
    ClientContext,
    GraphQLClient,
    useMutation,
    useQuery,
} from 'graphql-hooks'
import { Textarea } from '../node_modules/@canonical/react-components/dist/index';

const myProjectsQuery = `
    query {        
        projects(membership: true) {
            nodes {
                id
                name
            }
        }
    }
`;
export const issuesQuery = `
    query {
        project(fullPath: "fabrizio.armango/user-stories-repo1") {
            issues {
                nodes {
                    title
                    description
                    labels {
                        edges { 
                            node { title }
                        }
                    }
                }
            }
        }
    }
`;

export const createIssueMutation = `
    mutation UploadPostPicture($picture: Upload!) {
        uploadPostPicture(picture: $picture) {
            id
            pictureUrl
        }
    }
`;

const LABEL_USERSTORY = 'orcx::UserStory';
const LABEL_RISK_LOW = 'orcx::Risk:Low',
    LABEL_RISK_MODERATE = 'orcx::Risk:Moderate',
    LABEL_RISK_HIGH = 'orcx::Risk:High';

const LABEL_PRIORITY_MUSTDO = 'orcx::Priority:MustDo',
    LABEL_PRIORITY_SHOULDDO = 'orcx::Priority:ShouldDo',
    LABEL_PRIORITY_COULDDO = 'orcx::Priority:CouldDo';


function isUserStory(labels) {
    return labels.indexOf(LABEL_USERSTORY) != -1;
}


function parsePriority(labels) {
    if (labels.indexOf(LABEL_PRIORITY_MUSTDO) != -1) {
        return UserStoryPriority.MustDo;
    } else if (labels.indexOf(LABEL_PRIORITY_SHOULDDO) != -1) {
        return UserStoryPriority.ShouldDo;
    } else if (labels.indexOf(LABEL_PRIORITY_COULDDO) != -1) {
        return UserStoryPriority.CouldDo;
    }

    return UserStoryPriority.Undefined;
}

function parseRisk(labels) {
    if (labels.indexOf(LABEL_RISK_LOW) != -1) {
        return UserStoryRisk.Low;
    } else if (labels.indexOf(LABEL_RISK_MODERATE) != -1) {
        return UserStoryRisk.Moderate;
    } else if (labels.indexOf(LABEL_RISK_HIGH) != -1) {
        return UserStoryRisk.High;
    }

    return UserStoryRisk.Undefined;
}

export default function UserStories() {
    const client: GraphQLClient = useContext(ClientContext);
    const { loading, error, data, refetch } = useQuery(issuesQuery, {
        // refetchAfterMutations: createPostMutation
    });

    const { loading: myProjectsAreLoading, error: myProjectsError, data: myProjectsData, refetch: refetchMyProjects } = useQuery(myProjectsQuery);

    const [createIssue] = useMutation(createIssueMutation);
    const [userStories, setUserStories] = useState<UserStory[]>([]);
    const [projectOptions, setProjectOptions] = useState([]);

    const [formData, setFormData] = useState({
        gitlabEndpoint: "https://gitlab.com/api/v4/",
        personalAccessToken: "",
        userStoryTitle: "",
        userStoryDescription: "",
        userStoryPriority: "",
        userStoryRisk: "",
        projectId: ""

    });

    useEffect(() => {
        client.setHeader('Authorization', `Bearer ${formData.personalAccessToken}`)
        refetch();
        refetchMyProjects();
    }, [client]);

    useEffect(() => {
        console.log("myProjectsData: ", myProjectsData);
        if (!myProjectsData) return;
        const projects = myProjectsData.projects.nodes;
        setProjectOptions(projects.map(project => {
            const gid = project.id;
            const tmp = gid.split("/");
            return {
                label: project.name,
                value: tmp.pop()
            }
        }));
    }, [myProjectsData]);

    useEffect(() => {
        console.log("data: ", data);
        if (!data) return;

        const newUserStories = [];
        const issues = data.project.issues.nodes;
        issues.forEach((issue) => {
            const issueLabels = issue.labels.edges.map(x => x.node.title)
            console.log(issueLabels);

            if (!isUserStory(issueLabels)) {
                return;
            }
            const priority: UserStoryPriority = parsePriority(issueLabels);
            const risk: UserStoryRisk = parseRisk(issueLabels);

            const newUserStory: UserStory = {
                title: issue.title,
                priority: priority,
                risk: risk,
                description: issue.description
            }

            newUserStories.push(newUserStory);
        });

        setUserStories(newUserStories);
    }, [data]);

    const createNewUserStory = useCallback(() => {
        fetch(`https://gitlab.com/api/v4/projects/${formData.projectId}/issues`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${formData.personalAccessToken}`
            },
            body: JSON.stringify({
                id: "",
                title: `${formData.userStoryTitle} (${Date.now()})`,
                description: `${formData.userStoryDescription} (${Date.now()})`,
                labels: [
                    LABEL_USERSTORY,
                    ...[formData.userStoryPriority, formData.userStoryRisk].filter(x => x)
                ]
            })
        })
    }, [formData]);



    const handleUserStoryTitleChange = useCallback((e) => {
        console.log(e);
        setFormData({
            ...formData,
            userStoryTitle: e.target.value
        });
    }, [formData]);

    const handleUserStoryDescriptionChange = useCallback((e) => {
        console.log(e);
        setFormData({
            ...formData,
            userStoryDescription: e.target.value
        });
    }, [formData]);

    const handleUserStoryPriorityChange = useCallback((e) => {
        console.log(e);
        setFormData({
            ...formData,
            userStoryPriority: e.target.value
        });
    }, [formData]);

    const handleUserStoryRiskChange = useCallback((e) => {
        console.log(e);
        setFormData({
            ...formData,
            userStoryRisk: e.target.value
        });
    }, [formData]);

    const handlePersonalAccessTokenChange = useCallback((e) => {
        console.log(e);
        setFormData({
            ...formData,
            personalAccessToken: e.target.value
        });
    }, [formData]);

    const handleGitlabEndpointChange = useCallback((e) => {
        console.log(e);
        setFormData({
            ...formData,
            gitlabEndpoint: e.target.value
        });
    }, [formData]);

    const handleProjectChange = useCallback((e) => {
        console.log(e);
        setFormData({
            ...formData,
            projectId: e.target.value
        });
    }, [formData]);

    return (
        <Layout pageTitle="User Stories" headerButtonId="user-stories">
            <Head>
                <title>{siteTitle}</title>
            </Head>

            <Form>
                <fieldset>
                    <h2>Configure your GitLab repository</h2>
                    <div className="p-form--inline">
                        <Input
                            value={formData.gitlabEndpoint}
                            type="text"
                            label="GitLab API URL"
                            onChange={handleGitlabEndpointChange}
                        />
                        <Input
                            value={formData.personalAccessToken}
                            type="text"
                            label="Personal Access Token"
                            onChange={handlePersonalAccessTokenChange}
                        />
                        <Select
                            onChange={handleProjectChange}
                            label="Project" options={[
                                { value: "", disabled: false, label: "Select an option" },
                                ...projectOptions
                            ]}
                        />
                    </div>
                    <div className="u-align--left">
                        <Input
                            value={formData.userStoryTitle}
                            placeholder="User Story Title"
                            type="text"
                            label="Title"
                            onChange={handleUserStoryTitleChange}
                        />
                    </div>
                    <div className="u-align--left">
                        <Textarea
                            value={formData.userStoryDescription}
                            placeholder="Description"
                            label="Description"
                            onChange={handleUserStoryDescriptionChange}
                        />
                    </div>
                    <div className="p-form--inline">
                        <Select
                            onChange={handleUserStoryPriorityChange}
                            label="Priority" options={[
                                { value: "", disabled: false, label: "Select an option" },
                                {
                                    label: 'Must Do',
                                    value: LABEL_PRIORITY_MUSTDO
                                },
                                {
                                    label: 'Should Do',
                                    value: LABEL_PRIORITY_SHOULDDO
                                },
                                {
                                    label: 'Could Do',
                                    value: LABEL_PRIORITY_COULDDO
                                },
                            ]} />
                        <Select
                            onChange={handleUserStoryRiskChange}
                            label="Risk" options={[
                                { value: "", disabled: false, label: "Select an option" },
                                {
                                    label: 'Low',
                                    value: LABEL_RISK_LOW
                                },
                                {
                                    label: 'Moderate',
                                    value: LABEL_RISK_MODERATE
                                },
                                {
                                    label: 'High',
                                    value: LABEL_RISK_HIGH
                                },
                            ]} />
                    </div>
                    <Button onClick={() => {
                        createNewUserStory();
                    }}>create new</Button>
                </fieldset>
            </Form>

            <ul>
                {userStories.map((userStory, idx) => {
                    return <li key={idx}>
                        <h4>{userStory.title}</h4>
                        <p>{userStory.description}</p>
                        <Chip lead="Priority" value={userStory.priority.toString()} />
                    </li>
                })}
            </ul>

        </Layout>

    )

}