export enum UserStoryRisk {
    Undefined = 0,
    Low,
    Moderate,
    High
}
export const RiskText = {
    [UserStoryRisk.Undefined]: "Undefined",
    [UserStoryRisk.Low]: "Low",
    [UserStoryRisk.Moderate]: "Moderate",
    [UserStoryRisk.High]: "High"
}

export enum UserStoryPriority {
    Undefined = 0,
    MustDo,
    ShouldDo,
    CouldDo
}

export const PriorityText = {
    [UserStoryPriority.Undefined]: "Undefined",
    [UserStoryPriority.MustDo]: "Must Do",
    [UserStoryPriority.ShouldDo]: "Should Do",
    [UserStoryPriority.CouldDo]: "Could Do"
}

type UserStory = {
    title: string;
    description: string;
    priority: UserStoryPriority;
    risk: UserStoryRisk;
}

export default UserStory