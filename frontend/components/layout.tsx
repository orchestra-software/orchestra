import React, { ReactNode, useEffect, useState } from "react";
import Head from "next/head";
import styles from "./layout.module.scss";
import Link from "next/link";
import { ICONS, Button, Icon, List } from "@canonical/react-components";

import { useAuth, useSigninCheck, useUser } from "reactfire";

export const siteTitle = "Orchestra.software";

type LayoutProps = {
  children: ReactNode;
  pageTitle?: string;
  pageControls?: ReactNode;
  home?: boolean;
  headerButtonId?: string;
  breadcrumbs?: any;
};

export default function Layout({
  children,
  pageTitle = "",
  home,
  headerButtonId,
  pageControls,
  breadcrumbs,
}: LayoutProps) {
  const auth = useAuth();
  const { status, data: signInCheckResult } = useSigninCheck();
  const { data: user } = useUser();

  const [isDropdownWindowActive, setDropdownWindowActive] = useState(false);
  const [rightNavigationDropdownActive, setRightNavigationDropdownActive] =
    useState(false);
  return (
    <div className={styles.container}>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <meta
          property="og:image"
          content={`https://og-image.vercel.app/${encodeURI(
            siteTitle
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        />
        <meta name="og:title" content={siteTitle} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <header id="navigation" className="p-navigation is-dark">
        <div className="p-navigation__row--full-width">
          <div className="p-navigation__banner">
            <div className="p-navigation__logo">
              <Link href="/">
                <p className="p-navigation__item u-no-margin--bottom">
                  <a className="p-navigation__link">
                    <i>Orchestra</i>.<small>software</small>
                  </a>
                </p>
              </Link>
            </div>
            <a
              href="#navigation"
              className="p-navigation__toggle--open"
              title="menu"
            >
              Menu
            </a>
            <a
              href="#navigation-closed"
              className="p-navigation__toggle--close"
              title="close menu"
            >
              Close menu
            </a>
          </div>
          <nav className="p-navigation__nav" aria-label="Example main">
            <ul id="main-navigation" className="p-navigation__items u-clearfix">
              <li className={`p-navigation__item--dropdown-toggle`}>
                <Button
                  hasIcon
                  aria-controls="changemyid2"
                  className="p-navigation__link u-no-margin--right"
                  onClick={() => {
                    // setRightNavigationDropdownActive(!rightNavigationDropdownActive);
                    setDropdownWindowActive(!isDropdownWindowActive);
                  }}
                >
                  <Icon name={ICONS.chevronUp} light />
                  <span>Menu</span>
                </Button>
                <ul
                  aria-hidden="false"
                  className="p-navigation__dropdown p-navigation__dropdown--right"
                  id="changemyid2"
                ></ul>
              </li>
            </ul>
            {!signInCheckResult?.signedIn && (
              <Link href="/auth/sign-in">
                <a href="/auth/sign-in">Sign in</a>
              </Link>
            )}
            {signInCheckResult?.signedIn && (
              <ul className="p-navigation__items">
                <li
                  className={`p-navigation__item--dropdown-toggle${
                    rightNavigationDropdownActive ? " is-active" : ""
                  }`}
                >
                  <button
                    aria-controls="changemyid"
                    className="p-navigation__link u-no-margin--right"
                    onClick={() => {
                      setRightNavigationDropdownActive(
                        !rightNavigationDropdownActive
                      );
                    }}
                  >
                    {user?.displayName}
                  </button>
                  <ul
                    aria-hidden="false"
                    className="p-navigation__dropdown p-navigation__dropdown--right"
                    id="changemyid"
                  >
                    <li>
                      <a
                        href={`/api/auth/signout`}
                        className="p-navigation__dropdown-item"
                        onClick={(e) => {
                          e.preventDefault();
                          // signOut();
                        }}
                      >
                        Sign out
                      </a>
                    </li>
                  </ul>
                </li>
              </ul>
            )}
          </nav>
        </div>
      </header>
      {isDropdownWindowActive && (
        <div className="dropdown-window">
          <div className="" id="main-menu">
            <div className="dropdown-window__content u-no-padding--top u-no-padding--bottom">
              <div className="p-strip is-x-shallow u-no-padding--bottom">
                <div className="row u-equal-height">
                  <div className="col-medium-2 col-3 p-card--navigation">
                    <div className="u-hide--small">
                      <h4 className="is-dense">
                        <Link href="/manifests">
                          <a href="/manifests">Manifests&nbsp;›</a>
                        </Link>
                      </h4>
                      <p className="p-p--small">
                        Document your software with Orchestra.
                      </p>
                    </div>
                    <h4 className="u-show--small p-muted-heading u-hide--medium u-hide--large">
                      <Link href="/manifests">
                        <a href="/manifests">Manifests</a>
                      </Link>
                    </h4>
                    <List divided items={[]} />
                  </div>
                  <div className="col-medium-2 col-3 p-card--navigation">
                    <div className="u-hide--small">
                      <h4 className="is-dense">
                        <Link href="/diagrams">
                          <a href="/diagrams">Diagrams&nbsp;›</a>
                        </Link>
                      </h4>
                      <p className="p-p--small">
                        Write, view, collect and publish UML Diagrams.
                      </p>
                    </div>
                    <h4 className="u-show--small p-muted-heading u-hide--medium u-hide--large">
                      <Link href="/diagrams">
                        <a href="/diagrams">Diagrams</a>
                      </Link>
                    </h4>
                    <List divided items={[]}></List>
                  </div>
                  <div className="col-medium-2 col-3 p-card--navigation">
                    <div className="u-hide--small">
                      <h4 className="is-dense">
                        <Link href="/user-stories">
                          <a href="/user-stories">User stories&nbsp;›</a>
                        </Link>
                      </h4>
                      <p className="p-p--small">
                        Write the requirements, organize and reuse them.
                      </p>
                    </div>
                    <h4 className="u-show--small p-muted-heading u-hide--medium u-hide--large">
                      <Link href="/user-stories">
                        <a href="/user-stories">User stories</a>
                      </Link>
                    </h4>
                    <List divided items={[]} />
                  </div>
                  <div className="col-medium-2 col-3 p-card--navigation">
                    <div className="u-hide--small">
                      <h4 className="is-dense">
                        <Link href="/adrs">
                          <a href="/adrs">ADRs&nbsp;›</a>
                        </Link>
                      </h4>
                      <p className="p-p--small">
                        Architecture Decision Records.
                      </p>
                    </div>
                    <h4 className="u-show--small p-muted-heading u-hide--medium u-hide--large">
                      <Link href="/adrs">
                        <a href="/adrs">ADRs</a>
                      </Link>
                    </h4>
                    <List divided items={[]} />
                  </div>
                </div>
              </div>
              <div className="u-fixed-width u-hide--small">
                <hr className="u-no-margin--bottom" />
              </div>
              <div className="u-fixed-width">
                <hr className="p-hr--subtle p-hr--dense" />
              </div>
              <div className="row">
                <div className="col-3">
                  <h4 className="p-muted-heading--small">Support</h4>
                </div>
                <div className="col-9">
                  <ul className="p-inline-list--middot is-x-dense">
                    <li className="p-inline-list__item">
                      <Link href="/docs">
                        <a href="/docs">Read the docs</a>
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="u-fixed-width">
                <hr className="p-hr--subtle p-hr--dense" />
              </div>
              <div className="row">
                <div className="col-3">
                  <h4 className="p-muted-heading--small">Principles</h4>
                </div>
                <div className="col-9">
                  <ul className="p-inline-list--middot is-x-dense">
                    <Link href="/manifesto">
                      <a href="/manifesto">Read the Manifesto</a>
                    </Link>
                  </ul>
                </div>
              </div>
              <div className="u-fixed-width">
                <hr className="p-hr--subtle p-hr--dense" />
              </div>
              <div className="u-sv3">
                <div className="row">
                  <div className="col-3">
                    <Link href="/tools">
                      <a href="/tools">
                        <h4 className="p-muted-heading--small">Tools</h4>
                      </a>
                    </Link>
                  </div>
                  <div className="col-9">
                    <ul className="p-inline-list--middot is-x-dense">
                      <li className="p-inline-list__item">
                        <Link href="/tools/sprints">
                          <a href="/tools/sprints">
                            Create a release plan of sprints
                          </a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
      {breadcrumbs && (<>
        <nav className="p-breadcrumbs" aria-label="Breadcrumbs">
          <ol className="p-breadcrumbs__items">
            {breadcrumbs.map((x, idx) => {
              return (
                <li key={idx} className="p-breadcrumbs__item">
                  {x.route && <a href={x.route}>{x.label}</a>}
                  {!x.route && x.label}
                </li>
              );
            })}
          </ol>
        </nav>
        <hr />
        </>
      )}

      <main className="p-strip is-shallow">
        <div className="row">
          {(pageTitle || pageControls) && <div className="p-panel">
            <div className="p-panel__header">
              <h1 className="p-panel__title">{pageTitle}</h1>
              {pageControls && (
                <div className="p-panel__controls">{pageControls}</div>
              )}
            </div>
            <div className="p-panel__content"></div>
          </div>}
          {children}
        </div>
      </main>
      <footer className="l-footer p-strip--light u-no-padding--top u-no-padding--bottom">
        <nav className="row" aria-label="Footer">
          <div>
            v{process.env.NEXT_PUBLIC_ORCHESTRA_VERSION}
            <span className="u-off-screen">
              <a href="#">Go to the top of the page</a>
            </span>
          </div>
        </nav>
      </footer>
    </div>
  );
}
