// https://raw.githubusercontent.com/makerkit/ultimate-firebase-nextjs-authentication-guide/aadc35a189f62e4205a5d8326e654220a5e523b2/lib/hooks/useSignInWithProvider.ts
import { useAuth } from "reactfire";
import { FirebaseError } from "firebase/app";

import {
  AuthProvider,
  signInWithPopup,
  browserPopupRedirectResolver,
  UserCredential,
} from "firebase/auth";

import { useRequestState } from "./useRequestState";
import { useCallback } from "react";

export function useSignInWithProvider() {
  const auth = useAuth();

  const { state, setLoading, setData, setError } = useRequestState<
    UserCredential,
    FirebaseError
  >();

  const signInWithProvider = useCallback(async (provider: AuthProvider) => {
    setLoading(true);

    try {
      const credential = await signInWithPopup(
        auth,
        provider,
        browserPopupRedirectResolver
      );

      console.log(credential);

      setData(credential);
    } catch (error) {
      setError(error as FirebaseError);
    }
  }, [auth, setData, setError, setLoading]);

  return [signInWithProvider, state] as [
    typeof signInWithProvider,
    typeof state
  ];
}
