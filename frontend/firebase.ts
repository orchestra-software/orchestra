import { getApp, getApps, initializeApp } from "firebase/app"
import { connectAuthEmulator, getAuth } from "firebase/auth"
import { connectFirestoreEmulator, getFirestore } from "firebase/firestore"

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_CONFIG_APIKEY,
  authDomain: process.env.NEXT_PUBLIC_FIREBASE_CONFIG_AUTHDOMAIN,
  projectId: process.env.NEXT_PUBLIC_FIREBASE_CONFIG_PROJECTID,
  appId: process.env.NEXT_PUBLIC_FIREBASE_CONFIG_APPID,
  storageBucket: process.env.NEXT_PUBLIC_FIREBASE_CONFIG_,
  messagingSenderId: process.env.NEXT_PUBLIC_FIREBASE_CONFIG_MESSAGINGSENDERID
};

export const configuration = {
  firebase: firebaseConfig,
  emulatorHost: process.env.NEXT_PUBLIC_EMULATOR_HOST,
  emulator: process.env.NEXT_PUBLIC_EMULATOR === 'true',
  paths: {
    signIn: '/auth/sign-in',
    signUp: '/auth/sign-up',
    appHome: '/dashboard',
  },
  emulators: {
    auth: {
      port: 9099
    },
    firestore: {
      port: 8080
    }
  }
};


if (!getApps().length) {
  const app = initializeApp(configuration.firebase)

  const auth = getAuth(app),
    firestore = getFirestore(app)
    
  if (typeof window != "undefined" && process.env.NODE_ENV != "production") {
    console.info("Dev Env Detected: Using Emulators!")

    if (auth.emulatorConfig?.host !== "localhost")
      connectAuthEmulator(auth, `http://localhost:${configuration.emulators.auth.port}`, {
        disableWarnings: true,
      })

    // @ts-ignore
    if (!firestore._settings?.host.startsWith("localhost"))
      connectFirestoreEmulator(firestore, "localhost", configuration.emulators.firestore.port)
  }
}

export const app = getApp()
export const auth = getAuth(app)
export const firestore = getFirestore(app)